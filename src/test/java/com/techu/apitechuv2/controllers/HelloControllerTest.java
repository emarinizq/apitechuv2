package com.techu.apitechuv2.controllers;

import org.junit.Assert;
import org.junit.Test;

public class HelloControllerTest {

    @Test
    public void testHello(){
        HelloCtrl sut = new HelloCtrl();

        Assert.assertEquals("Hello USER", sut.hello("USER"));
    }
}
