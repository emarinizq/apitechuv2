package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll(){
        return this.productRepository.findAll();
    }

    public Optional<ProductModel> findById(String id){
        return this.productRepository.findById(id);
    }

    public ProductModel save(ProductModel product){
        return this.productRepository.save(product);
    }

    public void delete(String id){
        this.productRepository.deleteById(id);
    }
}
