package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2/products")
public class ProductCtrl {

    @Autowired
    ProductService productService;

    @GetMapping
    public ResponseEntity<List<ProductModel>> getProducts(){

        System.out.println("getProducts");

        return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel newProduct){

        System.out.println("createProducts");

        return new ResponseEntity<>(this.productService.save(newProduct), HttpStatus.CREATED );
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){

        System.out.println("getProducts");

        Optional<ProductModel> retrievedProduct = this.productService.findById(id);

        return new ResponseEntity<>(
            retrievedProduct.isPresent() ? retrievedProduct.get() : "Producto no encontrado",
            retrievedProduct.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateProduct(@PathVariable String id, @RequestBody ProductModel updateProduct){

        System.out.println("createProducts");

        Optional<ProductModel> response = this.productService.findById(id);

        if(!response.isPresent()){
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND );
        }

        ProductModel retrievedProduct = response.get();


        if(updateProduct.getDesc() != null){
            retrievedProduct.setDesc(updateProduct.getDesc());
        }

        if(updateProduct.getPrice() > 0){
            retrievedProduct.setPrice(updateProduct.getPrice());
        }

        return new ResponseEntity<>(this.productService.save(retrievedProduct), HttpStatus.OK);

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteProduct(@PathVariable String id){

        System.out.println("createProducts");

        Optional<ProductModel> response = this.productService.findById(id);

        if(!response.isPresent()){
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND );
        }else{
            this.productService.delete(id);
            return new ResponseEntity<>(response.get(), HttpStatus.OK);
        }

    }

}
